const express = require('express');
const webRTC = require('webrtc.io');

const app = express();

app.use('/', express.static('./Client'));

const webRTCServer = webRTC.listen(8001);

app.listen('8000', () => {
    console.log('Server Started');
})